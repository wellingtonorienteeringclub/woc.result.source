﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WOC.Result.Source {
  class Program {
    static async Task Main(string[] args) {
      string settingsFileName = "settings.json";

      if (!File.Exists(settingsFileName)) {
        File.WriteAllText(settingsFileName, JsonConvert.SerializeObject(new Settings {
          Path = "",
          EndPoint = "",
          IntervalInSeconds = 10
        }));
      }

      var settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(settingsFileName));

      IServiceCollection serviceCollection = new ServiceCollection();

      serviceCollection.AddLogging(builder => builder.AddConsole().AddFilter(level => level >= LogLevel.Information));

      var loggerFactory = serviceCollection.BuildServiceProvider().GetService<ILoggerFactory>();

      var clp = loggerFactory.CreateLogger("Program");

      clp.LogInformation($"Watching {settings.Path}...");

      var fileWatcher = new Dictionary<string, DateTime>();

      bool updateRightAway = false;

      while (true) {
        Thread.Sleep(settings.IntervalInSeconds * 1000);

        Stopwatch stopwatch = new Stopwatch();

        stopwatch.Start();

        bool update = false;

        var files = new List<string> { settings.Path };

        foreach (var file in files) {
          var fileInfo = new FileInfo(file);

          if (fileWatcher.ContainsKey(file)) {
            if (fileInfo.LastWriteTime != fileWatcher[file]) {
              fileWatcher[file] = fileInfo.LastWriteTime;
              update = true;
            }
          } else {
            fileWatcher.Add(file, fileInfo.LastWriteTime);
            update = true;
          }
        }

        if (update || updateRightAway) {
          updateRightAway = false;

          try {
            using (var memStream = new MemoryStream()) {
              using (FileStream fileStream = File.OpenRead(settings.Path)) {
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
              }

              memStream.Position = 0;

              try {
                var resp = await settings.EndPoint.PostMultipartAsync(mp => mp.AddFile("result-file", memStream, "results.xml"));
                clp.LogInformation($"Uploaded xml to {settings.EndPoint}");
              } catch (Exception ex) {
                clp.LogError(ex, $"Failed to upload results to {settings.EndPoint}.");
              }
            }
          } catch {
            updateRightAway = true;
          }
        }

        stopwatch.Stop();

        clp.LogDebug("Time elapsed: {0}", stopwatch.Elapsed);
      }
    }
  }
}