﻿namespace WOC.Result.Source {
  public class Settings {
    public string Path { get; set; }
    public string EndPoint { get; set; }
    public int IntervalInSeconds { get; set; }
  }
}
